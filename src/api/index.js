import axios from 'axios';


const clientId = "?client_id=QLCCujKeaqqw_c_a-Vc6ulnzqayfoL3Ubulxy3_0fQU"
const url = 'https://api.unsplash.com/topics/';

// Fetch all categories
export const fetchPictures = async() => {
    try{
        const { data } = await axios.get(`${url}/${clientId}`);
        const modifieldData = data.map((photo) => ({
            id: photo.id,
            slug: photo.slug,
            title: photo.title,
            description:photo.description,
            status : photo.status,
            starts: photo.starts,
            image: photo.preview_photos[0].urls.small,
        }))
        return modifieldData
    } catch(error) {
        console.log(error)
    }
}

// Fetch specifield categories
export const fetchCategories = async(category) => {

    const pictureUrl = `${url}/${category}/photos/${clientId}`;

    try{
        const { data } = await axios.get(pictureUrl);
        const actualData = data.map((category) => ({
            id: category.id,
            created_a: category.created_at,
            image: category.urls.small,
            description: category.description
        }))
        return actualData
    } catch(error) {
        console.log(error)
    }
}

