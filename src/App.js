import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import NavBar from './Components/NavBar';
import SideMenu from './Components/SideMenu';
import Category from './UI/Category';
import Categories from './UI/Categories';


const App = () => {
  return (
    <BrowserRouter>
     <NavBar />
     <SideMenu />
        <Routes>
              <Route path="/" element={<Category />} />
              <Route path="category/:slug" element={<Categories /> } />
        </Routes>

    </BrowserRouter> 
  );
}

export default App;
