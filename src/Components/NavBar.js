import React from 'react'
import "./Navbar.css";
import {Link} from 'react-router-dom'

const NavBar = () => {
  return (
    <nav className="navbar">
      <ul className="navbar__links">
        <li>
          <h1><Link to="/" className='navbar__logo'>Home : All categories</Link></h1> 
        </li>
      </ul>
    </nav>
  )
}
export default NavBar