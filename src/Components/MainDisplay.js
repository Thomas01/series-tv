import React from 'react'
import './MainDisplay.css'
import Tooltip from '@material-ui/core/Tooltip'
import Fade from '@material-ui/core/Fade';

const MainDisplay = ({imageUrl, title, description}) => {
  return (
      <div>
        <Tooltip
            placement="top-start"
            TransitionComponent={Fade} 
            TransitionProps={{ timeout: 600 }} 
            title={title ? title : description || "Approved"}>
           <div className="picture">
           <img src={imageUrl} alt="" />
          </div>
        </Tooltip>
      </div>
  )
}

export default MainDisplay