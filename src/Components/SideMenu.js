import React,{useState, useEffect} from 'react'
import "./SideMenu.css"
import { Link } from "react-router-dom";
import { fetchPictures } from '../api';


const SideMenu = () => {
  
  const[images, setImages] = useState([]) 

  useEffect(() => {
    const fetchData = async() => {
    setImages(await fetchPictures());
    }
    fetchData()
  },[setImages])

  // Side bar buttons on small screens
  const OpenSidebar = () =>{
  document.querySelector(".side-container").style.display = "flex";
  }
 
  const CloseSidebar = () => {
  document.querySelector(".side-container").style.display = "none";
  }

  return (
    <div>
    <span className="sideIcon" onClick={()=>OpenSidebar()}>&#9776;</span>
    <div className="side-container">
      <ul className="side-nav">
      <span className="closebtn" onClick={()=>CloseSidebar()}>&times;</span>
        {
          images && images.map((image) => 
          <li key={image.id}>
             <Link to={`/category/${image.slug}`}>
               <span className="text">{image.title}</span>
             </Link >
          </li>
          )}
      </ul>
    </div> 
    </div> 
  )
}
export default SideMenu