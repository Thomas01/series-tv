
// Horizontal scroll effect
let container = document.querySelector("mainscreen").innerWidth()
let indev = document.querySelector("mainscreen__pictures").innerWidth()
let inW = indev.outerWidth()
container.mousemove((e)=>{
    indev.css({left: ((container - inW)*((e.pageX / container).toFixed(3))).toFixed(1) +"px" })
})