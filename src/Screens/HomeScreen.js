import React, {useState, useEffect} from 'react'
import {fetchPictures} from '../api'
import MainDisplay from '../Components/MainDisplay'
import LinearProgress from '@material-ui/core/LinearProgress';

const HomeScreen = () => {

  const[photos, setPhotos] = useState([])
  
  useEffect(() => {
    const fetchData = async() => {
    setPhotos(await fetchPictures());
    }
    fetchData()
  }, [setPhotos])

  return (
    <div className="mainscreen">
        <div className="mainscreen__pictures">
         {
          !photos.length ? <LinearProgress /> : photos.map((picture) => (
              <MainDisplay 
                 key={picture.id}
                 imageUrl={picture.image}
                 title={picture.title}
              />
            )) 
          }
       </div>
    </div>
  )
}

export default HomeScreen