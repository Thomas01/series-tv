import React, {useEffect, useState} from 'react'
import './MainScreen.css'
import MainDisplay from '../Components/MainDisplay'
import { useParams } from "react-router-dom"
import {fetchCategories} from '../api'
import LinearProgress from '@material-ui/core/LinearProgress';



const MainScreen = () => {

  const[pictures, setPicture] = useState([])
  const { slug } = useParams()

 // Fetch all pictures
  useEffect(() => {
    const fetchPicture = async() => {
    setPicture(await fetchCategories(slug));
    }
    fetchPicture()
}, [slug])

  return (
      <div className="mainscreen">
        <div className="mainscreen__pictures">
          {
             !pictures.length  ? <LinearProgress className="loading"/> : pictures.map((picture) => (
                <MainDisplay
                  key={picture.id}
                  description={picture.description}
                  imageUrl={picture.image}         
               />
             )) 
          }
       </div>
    </div>
  )
}

export default MainScreen